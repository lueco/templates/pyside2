#! /usr/bin/python3
# This Python file uses the following encoding: utf-8

__version__ = '0.0.0'
__channel__ = 'dev'
__title__ = 'lu Templates PySide2'

print('{title} v{version} {channel}'.format(title=__title__, version=__version__, channel=__channel__))
print('Importing core libraries...')
import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
print('Done.')
print('Importing additional libraries...')
from PySide2.QtWebEngineWidgets import *
from mainwindow_ui import *
print('Done.')


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    window.authenticate()
#    dialog = AuthDialog()
#    dialog.show()
    sys.exit(app.exec_())
